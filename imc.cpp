#include <iostream>
#include <iomanip>

using namespace std;

int main(void){

	unsigned short peso;	// Declaro variable entera para almacenar peso.
	float estatura;		// Declaro variable flotante para almacenar estatura.

	cout << "Ingresa peso (Kg): ";	// Pregunto a usuario por peso.
	cin >> peso;			// Obtengo de usuario el peso y asigno valor a variable peso.

	cout << "Ingresa estatura (m): ";	// Pregunto a usuario por estatura.
	cin >> estatura;			// Obtengo de usuario la estatura y asigno valor a variable estatura.

	float imc;				// Declaro variable flotante para almacenar imc.
	imc = peso / (estatura * estatura);	// Asigno a variable imc el resultado de peso / (estatura * estatura).

	cout << endl << "IMC: " << fixed << setprecision(1) << imc << endl
		<< endl;			// Imprimo en pantalla el valor de imc.

	// Imprimo tabla con categorías de estado de peso estándar.
	cout << "+--------------------+---------------+" << endl;
	cout << "| MC                 | Nivel de peso |" << endl;
	cout << "| ------------------ | ------------- |" << endl;
	cout << "| Por debajo de 18.5 | Bajo peso     |" << endl;
	cout << "| 18.5 – 24.9        | Normal        |" << endl;
	cout << "| 25.0 – 29.9        | Sobrepeso     |" << endl;
	cout << "| 30.0 o más         | Obesidad      |" << endl;
	cout << "+--------------------+---------------+" << endl;

	return 0;
}
