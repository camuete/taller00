#include <stdio.h>

int main(void){

	unsigned short peso;	// Declaro variable entera para almacenar peso.
	float estatura;		// Declaro variable flotante para almacenar estatura.

	puts("Ingresa peso (Kg):");	// Pregunto a usuario por peso.
	scanf("%hu", &peso);		// Obtengo de usuario el peso y asigno valor a variable peso.

	puts("Ingresa estatura (m):");	// Pregunto a usuario por estatura.
	scanf("%f", &estatura);		// Obtengo de usuario la estatura y asigno valor a variable estatura.

	float imc;				// Declaro variable flotante para almacenar imc.
	imc = peso / (estatura * estatura);	// Asigno a variable imc el resultado de peso / (estatura * estatura).

	printf("\nIMC: %.1f\n\n", imc);	// Imprimo en pantalla el valor de imc.

	// Imprimo tabla con categorías de estado de peso estándar.
	puts("+--------------------+---------------+");
	puts("| MC                 | Nivel de peso |");
	puts("| ------------------ | ------------- |");
	puts("| Por debajo de 18.5 | Bajo peso     |");
	puts("| 18.5 – 24.9        | Normal        |");
	puts("| 25.0 – 29.9        | Sobrepeso     |");
	puts("| 30.0 o más         | Obesidad      |");
	puts("+--------------------+---------------+");

	return 0;
}
