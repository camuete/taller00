# Taller00 - Calculadora IMC

Escriba un programa que le pida al usuario que ingrese su estatura en metros
(m) y su peso en kilogramos (kg). El programa debe calcular el índice de masa
corporal (IMC)[^1] con los datos ingresados por el usuario usando la
siguiente formula:

```math
IMC = \frac{Peso (kg)}{Estatura (m)^2}
```

Luego, el programa debe imprimir el resultado en pantalla junto a una tabla con
los siguientes rangos:

| MC                  | Nivel de peso |
| :---                | :---          |
| Por debajo de 18.5  | Bajo peso     |
| 18.5 – 24.9         | Normal        |
| 25.0 – 29.9         | Sobrepeso     |
| 30.0 o más          | Obesidad      |

Tabla: Categorías de estado de peso estándar.

## Refinamiento 1

1. Preguntar y obtener del usuario el peso y la estatura.
1. Calcular e imprimir el IMC.
1. Imprimir la tabla con categorías de estado de peso estándar.

## Refinamiento 2

1. Declaro variable entera para almacenar `peso`.
1. Declaro variable flotante para almacenar `estatura`.
1. Pregunto a usuario por peso.
1. Obtengo de usuario el peso y asigno valor a variable `peso`.
1. Pregunto a usuario por estatura.
1. Obtengo de usuario la estatura y asigno valor a variable `estatura`.
1. Declaro variable flotante para almacenar `imc`.
1. Asigno a variable `imc` el resultado de `peso / (estatura * estatura)`.
1. Imprimo en pantalla el valor de `imc`.
1. Imprimo tabla con categorías de estado de peso estándar.

# Referencias

[^1]: HHS. G. –Departamento de Salud y Servicios Humanos, «Acerca del índice de
  masa corporal para adultos».
  https://www.cdc.gov/healthyweight/spanish/assessing/bmi/adult_bmi/index.html, 2022. 
